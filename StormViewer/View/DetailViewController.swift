//
//  DetailViewController.swift
//  Storm_Viewer
//
//  Created by Hariharan S on 30/04/24.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: - IBOutlet

    @IBOutlet var imageView: UIImageView!
    
    // MARK: - Property
    
    var selectedImage: String?
    var selectedPictureCount: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.diaplayImage()
        self.configureNavBar()
    }
}

// MARK: - Private Methods

private extension DetailViewController {
    func configureNavBar() {
        self.title = "Picture \(self.selectedPictureCount) of 10"
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(self.shareButtonTapped)
        )
    }
    
    func diaplayImage() {
        guard let imageName = self.selectedImage
        else {
            return
        }
        self.imageView.image = UIImage(named: imageName)
    }
    
    @objc func shareButtonTapped() {
        guard let image = self.imageView.image?.jpegData(compressionQuality: 1.0)
        else {
            fatalError("No image found")
        }
        
        let activityVC = UIActivityViewController(
            activityItems: [image, self.selectedImage as Any],
            applicationActivities: []
        )
        activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        self.present(activityVC, animated: true)
    }
}
