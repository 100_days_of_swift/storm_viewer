//
//  ViewController.swift
//  Storm_Viewer
//
//  Created by Hariharan S on 28/04/24.
//

import UIKit

class ViewController: UITableViewController {
    var pictureNames: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadImages()
        self.configureNavBar()
    }
}


// MARK: - Private Methods

private extension ViewController {
    func configureNavBar() {
        self.title = "Storm Viewer"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(self.shareButtonTapped)
        )
    }
    
    func loadImages() {
        let fileDirectory = FileManager.default
        DispatchQueue.global(qos: .background).async {
            guard let resourcePath = Bundle.main.resourcePath,
                  let imageItems = try? fileDirectory.contentsOfDirectory(atPath: resourcePath)
            else {
                print("Didn't find the Resource path")
                return
            }
            
            for imageName in imageItems where imageName.hasPrefix("nssl") {
                self.pictureNames.append(imageName)
            }
        }
        
        DispatchQueue.main.async {
            self.pictureNames.sort()
            self.tableView.reloadData()
        }
    }
    
    @objc func shareButtonTapped() {
        let items = ["Share the App"]
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        self.present(activityVC, animated: true)
    }
}

// MARK: - UITableViewDelegate Conformance

extension ViewController {
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        guard let detailVC = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController
        else {
            return
        }
        detailVC.selectedPictureCount = indexPath.row + 1
        detailVC.selectedImage = pictureNames[indexPath.row]
        let count: Int = UserDefaults.standard.integer(forKey: pictureNames[indexPath.row])
        UserDefaults.standard.setValue(count + 1, forKey: pictureNames[indexPath.row])
        self.navigationController?.pushViewController(detailVC, animated: true)
        self.tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource Conformance

extension ViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        self.pictureNames.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
        cell.textLabel?.text = pictureNames[indexPath.row]
        let count = UserDefaults.standard.integer(forKey: pictureNames[indexPath.row])
        cell.detailTextLabel?.text = "Total Views: \(count)"
        return cell
    }
}
